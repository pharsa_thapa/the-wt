"""
Database model for Online Store

Provides functions to access the database
"""


def product_get(db, id):
    """Return the product with the given id or None if
    it can't be found.
    Returns a sqlite3.Row object"""

    sql = """SELECT id, name, description, category, image_url, unit_cost, inventory FROM products WHERE id=?"""
    cur = db.cursor()
    cur.execute(sql, (id,))

    return cur.fetchone()


def product_list(db, category=None):
    """Return a list of products, if category is not None, return products from
    that category. Results are returned in no particular order.
    Returns a list of tuples (id, name, description, category, image_url, unit_cost, inventory)"""

    cur = db.cursor()

    if category:
        sql = """SELECT id, name, description, category, image_url, unit_cost, inventory 
        FROM products WHERE category = ?
        """
        cur.execute(sql, (category,))
    else:
        sql = 'SELECT id, name, description, category, image_url, unit_cost, inventory FROM products'
        cur.execute(sql)

    return cur.fetchall()


def save_session_data(db, session_id, json_data):
    """This functions saves session records into database especially when creating new record"""
    cur = db.cursor()
    sql = """INSERT INTO sessions (sessionid, data) VALUES (?, ?)"""
    cur.execute(sql, [session_id, json_data])


def update_session_data(db, session_id, json_data):
    """THis function updates session records especially while updating cart details"""
    cur = db.cursor()
    sql = """UPDATE sessions SET data = ? WHERE sessionid = ?"""
    cur.execute(sql, [json_data, session_id, ])


def get_saved_session(db, session_id):
    """This function returns an instance of saved session data
        sessionid and data
    """
    cur = db.cursor()
    sql = "SELECT * FROM sessions WHERE sessionid = ?"
    cur.execute(sql, [session_id])
    return cur.fetchone()
