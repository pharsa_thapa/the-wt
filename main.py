
import random
import json
from bottle import Bottle, template, static_file, request, redirect, HTTPError, response

import model
import session
import cart_helper

app = Bottle()


# id, name, description, image_url, category, inventory, unit_cost

@app.route('/')
def index(db):
    """Load home page"""
    #set up cookies
    cookie = session.get_or_create_session(db)

    #Retrieve all the products in db
    all_products = model.product_list(db)
    info = {
        'title': "The WT Store",
        'products': all_products,
        'page_heading': 'Our Products'
    }
    return template('index', info)


@app.route('/about')
def about():
    """Load about us page with message"""
    message = 'Welcome to WT! The innovative online store'
    info = {
        'message': message
    }
    return template('about', info)


@app.route('/category/<category>')
def get_products_by_category(db, category):
    """Load all the products in the category
        category: encoded in url path could be men or women or anything
    """
    #check if category is neither men or women
    if category not in ["men", "women"]:
        #setting up response for raising error
        response.status = 302
        response.body = "No products in this category"
        response.headers['Location'] = '/category/error'
        response.content_type = 'text/html;'
        return response

    else:
        #find all the products in the given category
        products_in_category = model.product_list(db, category=category)
        info = {
            'title': "The WT Store",
            'products': products_in_category,
            #setting up dynamic heading for the page
            'page_heading': 'For Men' if category == 'men' else 'For Women'
        }

        return template('index', info)


@app.route('/category/error')
def category_not_found():
    """Raised category not found error handled"""
    info = {
        'message': "Category not found!",
    }
    return template('invalid_message', info)



@app.route('/product/<id>')
def get_product_by_id(db,id):
    """Load the product detail page
           id: encoded in url path could be any integer
    """
    #error handled for not supplying id
    if id == "":
        info = {
            'message': "Product not found!",
        }
        return template('invalid_message', info)
    else:
        #retrieve product details for the given product id
        product = model.product_get(db, id=id)

    #error handled for not found producct details in db
    if product == None:
        response.status = 404
        response.headers['Location'] = '/product/error'
        response.body = "Product not found"
        return response

    else:
        #setting up session in case of product being found
        session.get_or_create_session(db)
        info = {
            'product': product,
            'page_heading': product['name']
        }
        return template('product_detail', info)


@app.route('/product/error')
def product_not_found():
    """This functions is landing page for product being not found"""
    info = {
        'message': "Product not found!",
    }
    return template('invalid_message', info)


@app.route('/cart', 'GET')
def view_cart(db):
    """This functions loads the cart data saved for the session and visualise"""

    #setting variables' default values
    cart_has_items = False
    cart_total = 0
    cart_items = []

    session_record = session.get_cart_contents(db)

    #calculate cart total
    cart_total = cart_helper.decode_cart_json_get_total(session_record)

    #check if cart details has items in it or not, to fork on the visualization on cart page
    cart_has_items = True if len(session_record) > 0 else False

    info = {
        'cart_has_items': cart_has_items,
        'cart_total': cart_total,
        'cart_items': session_record
    }
    return template('cart', info)


@app.route('/cart', method='POST')
def read_cart_form(db):
    """THis function reveives form data and saves details in database """

    #retrieving form data
    item_id = request.forms.get("product")
    quantity = request.forms.get("quantity")

    #attempt to find and retrive product detail
    item = model.product_get(db, id=item_id)

    #error handling in case of invalid id provided in the request form
    if item == None:
        response.status = 404
        response.headers['Location'] = '/product/error'
        return response
    else:
        #valid data being saved in db
        session.add_to_cart(db, item_id, quantity)

    #redirecting to cart page after successful insertion of data
    response.status = 302
    response.headers['Location'] = '/cart'



@app.route('/static/<filename:path>')
def static(filename):
    """This function is used to link static files for eg: css files"""
    return static_file(filename=filename, root='static')


@app.error(404)  # changed from OP
def error404(error):
    #custom 404 error page
    info = {
        'message': "404, Page not found!",
    }
    return template('invalid_message', info)


if __name__ == '__main__':

    from bottle.ext import sqlite
    from dbschema import DATABASE_NAME
    # install the database plugin
    app.install(sqlite.Plugin(dbfile=DATABASE_NAME))
    app.run(debug=True, port=8010)
