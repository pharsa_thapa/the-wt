
def decode_cart_json_get_total(decoded_cart_content):
    """This function takes decoded cart data and calculate total for the cart"""
    cart_total = 0

    #loop over all the dictionaries of items in cart data
    for item in decoded_cart_content:
        cart_total += float(item.get('sub_total'))

    #return formatted into 2 decimal numbers cart total
    return "%.2f" % cart_total

