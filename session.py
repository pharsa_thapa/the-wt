"""
Code for handling sessions in our web application
"""

from bottle import request, response
import uuid
import json

import model
import cart_helper
import dbschema

COOKIE_NAME = 'session'


def get_or_create_session(db):
    """Return the current sessionid either from a
    cookie in the current request or by creating a
    new session if none are present."""

    #get existing cookie
    cookie_id = request.get_cookie(COOKIE_NAME)

    #attempt to retrive saved cookie detail with the same cookie id
    session_data = model.get_saved_session(db, cookie_id)

    #session record not found
    if session_data == None:
        cart = []
        #generate new cookie key
        cookie_id = str(uuid.uuid4().hex)

        #set up generated cookie key in response
        response.set_cookie(COOKIE_NAME, cookie_id, path='/')

        #preparing session cart data to save
        data = json.dumps(cart)
        #save session detail
        model.save_session_data(db, cookie_id, data)
    else:
        cookie_id = session_data['sessionid']

    return cookie_id


def add_to_cart(db, itemid, quantity):
    """This function adds an item to the shopping cart while returning nothing"""

    #retrieving saved cart details
    cookie_id = request.get_cookie(COOKIE_NAME)
    saved_session = model.get_saved_session(db, cookie_id)

    saved_cart = json.loads(saved_session['data'])
    counter = 0

    product = model.product_get(db, itemid)

    #preparing new record to append in saved data
    new_item_dictionary = {
        'id': itemid,
        'quantity': int(quantity),
        'sub_total': "%.2f" % (int(quantity) * float(product['unit_cost'])), #subtotal = quantity X unit cost and format in 2 decimal places
        'name': product['name'],
        'cost': (float(product['unit_cost']))
    }

    for item in saved_cart:
        #check if item is already being enlisted in the cart detail
        if itemid == item.get('id'):
            previous_quantity = item.get('quantity')

            #updating new details for quantity and subtotal for the newly placed add item request
            new_quantity = int(previous_quantity) + int(quantity)
            new_sub_total = new_quantity * float(product['unit_cost'])
            new_item_dictionary.update({"quantity" : new_quantity, 'sub_total': new_sub_total})

            #removing the old saved detail for the existing item
            saved_cart.pop(counter)
        counter += 1

    #append newly modified details for the submitted item
    saved_cart.append(new_item_dictionary)
    json_cart = json.dumps(saved_cart)

    #update the session data
    model.update_session_data(db, cookie_id, json_cart)


def get_cart_contents(db):
    """Return the contents of the shopping cart as
    a list of dictionaries:
    [{'id': <id>, 'quantity': <qty>, 'name': <name>, 'cost': <cost>}, ...]
    """
    saved_cart = model.get_saved_session(db, request.get_cookie(COOKIE_NAME))
    return json.loads(saved_cart['data'])

